<?php

interface ShippingInterface
{
    /**
     * @return bool
     */
    public function send(): bool;
}