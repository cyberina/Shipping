<?php
class Sefareshi extends Shipping{

    /**
     * @return bool
     */
    public function send(): bool
    {
        echo 'Burden is sent through Sefareshi'.PHP_EOL;
        return true;
    }
}