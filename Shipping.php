<?php

abstract class Shipping implements ShippingInterface
{
    protected $shipping_name;
    protected $weight;
    protected $address;


    /**
     * Shipping constructor.
     * @param float $weight Shipment Weight In KM
     * @param string $address Destination's Full Direction
     * @param int $postal_code
     * @throws Exception
     */
    public function __construct(float $weight, string $address, int $postal_code)
    {
        $this->weight = $weight;
        $this->address = $address;
        if (is_null($this->shipping_name)) {
            $this->shipping_name = get_class($this);
        }
        if ($this->send()) {
            $this->storeInDB();
        } else {
            throw new Exception("Shipping wasn't successfully");
        }
    }

    /**
     * Store Payment In DataBase
     */
    private function storeInDB()
    {
        echo $this->shipping_name . ' :Shipping stored in database'.PHP_EOL;
    }
}