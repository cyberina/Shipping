<?php
class Express extends Shipping{

    /**
     * @return bool
     */
    public function send(): bool
    {
        echo 'Burden is sent through Express'.PHP_EOL;
        return true;
    }
}